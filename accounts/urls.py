from django.urls import path, include
from .views import SignIn, CreateUser, logout_

urlpatterns = [
    path('login/', SignIn.as_view(), name='sign-in'),
    path('signup/', CreateUser.as_view(), name='sign-up'),
    path('logout/', logout_, name='sign-out')
]

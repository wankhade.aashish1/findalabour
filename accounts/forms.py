from django import forms

class LoginForm(forms.Form):
    email = forms.EmailField(label='Email address')
    password = forms.CharField(label='Password', widget=forms.PasswordInput())

class SignupForm(forms.Form):
    first_name = forms.CharField(label='First Name', required=True)
    last_name = forms.CharField(label='Last Name', required=True)
    email = forms.EmailField(label='Email address', required=True)
    password = forms.CharField(min_length=8, label='Password', widget=forms.PasswordInput(), required=True)
    confirm_password = forms.CharField(min_length=8, label='Confirm Password', widget=forms.PasswordInput(), required=True)
    
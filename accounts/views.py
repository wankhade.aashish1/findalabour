from django.shortcuts import render
from django.views import View
from django.http import HttpResponseRedirect
from .forms import LoginForm, SignupForm
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from .models import User
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse
from django.shortcuts import redirect


# Create your views here.

class SignIn(View):
    template_name = 'accounts/login.html'
    form_class = LoginForm

    def get(self, request):
        if self.request.user.is_authenticated:
            return redirect('/success')
        form = self.form_class()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        if self.request.user.is_authenticated:
            return redirect('/success')
        form = self.form_class(request.POST)
        if form.is_valid():
            user = authenticate(request, **form.cleaned_data)
            if user is not None:
                login(request, user)
                return HttpResponseRedirect('/success/')
            else:
                return render(request, self.template_name, {'form': form, 'msg': 'Invalid Credentials'})

        return render(request, self.template_name, {'form': form})


class CreateUser(View):
    template_name = 'accounts/signup.html'
    form_class = SignupForm
    def get(self, request):
        if self.request.user.is_authenticated:
            return redirect('/success')
        form = self.form_class()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        if self.request.user.is_authenticated:
            return redirect('/success')
        form = self.form_class(request.POST)
        if form.is_valid():
            try:
                assert form.cleaned_data['password'] == form.cleaned_data['confirm_password']
                del form.cleaned_data['confirm_password']
                passwd = form.cleaned_data['password']
                del form.cleaned_data['password']
                user = User.objects.create(**form.cleaned_data)
                user.set_password(passwd)
                user.save()
                return HttpResponseRedirect('/success/')
            except AssertionError:
                form.fields['password'].help_text = "Password Didn't Matched"
                return render(request, self.template_name, {'form': form})

        return render(request, self.template_name, {'form': form})

def logout_(request):
    logout(request)
    return redirect(reverse('sign-in'))